/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.shapeproject;

import java.awt.Color;
import static java.awt.Color.GREEN;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Maintory_
 */
public class SquareFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Square");
        frame.setSize(300,120);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        JLabel lblRadius = new JLabel("Side: " , JLabel.TRAILING);
        lblRadius.setSize(50,20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.YELLOW);
        lblRadius.setOpaque(true);


        
        
       final  JTextField txtRadius = new JTextField();
       txtRadius.setSize(70,20);
       txtRadius.setLocation(60,5);
       
       frame.add(lblRadius);
       frame.add(txtRadius);
       
       final JButton btnCalculate = new JButton("Calculate");
       btnCalculate.setSize(100,20);
       btnCalculate.setLocation(140,5);
       btnCalculate.setOpaque(true);
       frame.add(btnCalculate);
       
       final JLabel lblResult = new JLabel(" Circle redius = ??? area = ?? perimeter = ??");
       lblResult.setHorizontalTextPosition(JLabel.CENTER);
       lblResult.setSize(500,50);
       lblResult.setLocation(0,30);
       lblResult.setBackground(GREEN);
       lblResult.setOpaque(true);
       frame.add(lblResult);
       
       btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                System.out.println(".actionPerformed()");
                String strRadius = txtRadius.getText();
                double side = Double.parseDouble(strRadius);
                Square square = new Square(side);
                lblResult.setText("Square Side = "
                        + square.getSide()
                        + " Area"
                        + String.format("%.2f",square.calArea())
                        +" Perimeter" 
                        + String.format("%.2f",square.calPerimeter()));
                }
                catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "PLEASE INPUT ONLY NUMBERS");
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
       
       frame.setVisible(true);
    }
    
}
