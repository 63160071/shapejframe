/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.shapeproject;

import java.awt.Color;
import static java.awt.Color.GREEN;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Maintory_
 */
public class TriangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(400,120);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        JLabel lblRadius = new JLabel("Base: " , JLabel.TRAILING);
        lblRadius.setSize(50,20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.YELLOW);
        lblRadius.setOpaque(true);
        
        JLabel lblWidth = new JLabel("Height: " , JLabel.TRAILING);
        lblWidth.setSize(50,20);
        lblWidth.setLocation(120, 5);
        lblWidth.setBackground(Color.PINK);
        lblWidth.setOpaque(true);
        
        frame.add(lblWidth);


        
        
       final  JTextField txtRadius = new JTextField();
       final JTextField txtWidth = new JTextField();
       txtRadius.setSize(50,20);
       txtRadius.setLocation(60,5);
       
       txtWidth.setSize(50,20);
       txtWidth.setLocation(180,5);
       
       frame.add(lblRadius);
       frame.add(txtRadius);
       frame.add(txtWidth);
       
       final JButton btnCalculate = new JButton("Calculate");
       btnCalculate.setSize(100,20);
       btnCalculate.setLocation(300,5);
       btnCalculate.setOpaque(true);
       frame.add(btnCalculate);
       
       final JLabel lblResult = new JLabel("Put some number");
       lblResult.setHorizontalTextPosition(JLabel.CENTER);
       lblResult.setSize(500,50);
       lblResult.setLocation(0,30);
       lblResult.setBackground(GREEN);
       lblResult.setOpaque(true);
       frame.add(lblResult);
       
       btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                System.out.println(".actionPerformed()");
                String strRadius = txtRadius.getText();
                String strWidth = txtWidth.getText();
                double base = Double.parseDouble(strRadius);
                double height = Double.parseDouble(strWidth);
                Triangle triangle = new Triangle(base,height);
                lblResult.setText("Base = "
                        + triangle.getBase() + triangle.getHeight()
                        + " Area"
                        + String.format("%.2f",triangle.calArea())
                        +" Perimeter" 
                        + String.format("%.2f",triangle.calPerimeter()));
                }
                catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "PLEASE INPUT ONLY NUMBERS");
                    txtRadius.setText("");
                    txtWidth.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
       
       frame.setVisible(true);
    }
}
