/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.shapeproject;

/**
 *
 * @author Maintory_
 */
public class Square extends Shape {
    private double side;
    private double width;

    public double getSide() {
        return side;
    }
    
    public double getWidth() {
        return side;
    }
    
    public void setWidth() {
        this.width = width;
    }

    public void setSide(double side) {
        this.side = side;
    }
    
    public Square(double side) {
        super("Suqare");
        this.side = side;
    }

    @Override
    public double calArea() {
        return Math.pow(side,2);
    }

    @Override
    public double calPerimeter() {
        return (side * 4);
    }
    
}
