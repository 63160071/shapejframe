/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.shapeproject;

/**
 *
 * @author Maintory_
 */
public class Rectangle extends Shape {

    private double width;
    private double side;
    
    public Rectangle(double side, double width) {
        super("Rectangle");
        this.side = side;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    @Override
    public double calArea() {
        return width*side;
    }

    @Override
    public double calPerimeter() {
        return 2*(width*side);
    }
}
    
